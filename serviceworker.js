// https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API/Using_Service_Workers
// https://github.com/mdn/sw-test

var CACHE = 'rrt-visualization-v6'

var filesToCache = [
  '/RRT-Visualization/',
  '/RRT-Visualization/index.html',
  '/RRT-Visualization/style.css',
  '/RRT-Visualization/js/main.js',
  '/RRT-Visualization/js/Rrt.js',
  '/RRT-Visualization/js/RrtCollisionChecker.js',
  '/RRT-Visualization/js/RrtApp.js',
  '/RRT-Visualization/js/RrtGui.js',
  '/RRT-Visualization/vendor/dat.gui.min.js',
  '/RRT-Visualization/vendor/threejs/three.min.js',
  '/RRT-Visualization/vendor/threejs/three.module.js',
  '/RRT-Visualization/vendor/threejs/Detector.js',
  '/RRT-Visualization/vendor/threejs/controls/OrbitControls.js',
  '/RRT-Visualization/icons/icon-32.png',
  '/RRT-Visualization/icons/icon-64.png',
  '/RRT-Visualization/icons/icon-128.png'
]

this.addEventListener('install', function (event) {
  event.waitUntil(
    caches.open(CACHE).then(function (cache) {
      return cache.addAll(filesToCache)
    })
  )
})

this.addEventListener('activate', function (event) {
  event.waitUntil(
    caches.keys().then(function (keyList) {
      return Promise.all(keyList.map(function (key) {
        if (key !== CACHE) {
          return caches.delete(key)
        }
      }))
    })
  )
  return self.clients.claim()
})

this.addEventListener('fetch', function (event) {
  // app shell files
  event.respondWith(
    caches.match(event.request).then(function (resp) {
      return resp || fetch(event.request)
    })
  )
})
