'use strict'

/**
 * @author Manfred Kröhnert
 */

function RrtGui () {
  var controls = {
    gui: null,
    'Delta': 1.0,
    'Volume Size': 40,
    'Max. Samples': 10000
  }

  this.getDelta = function () {
    return controls['Delta']
  }

  this.getVolumeSize = function () {
    return controls['Volume Size']
  }

  this.getMaxSamples = function () {
    return controls['Max. Samples']
  }

  var init = function () {
    controls.gui = new dat.GUI()

    var playback = controls.gui.addFolder('Playback')
    var settings = controls.gui.addFolder('Settings')

    playback
      .add(controls, 'start')
      .name('Plan/Pause')
    playback
      .add(controls, 'reset')
      .name('Reset')

    settings
      .add(controls, 'Delta', 0.1, 5, 0.01)
      .onChange(controls.deltaChanged)
    settings
      .add(controls, 'Volume Size', 5, 70, 1)
      .onChange(controls.volumeSizeChanged)
    settings
      .add(controls, 'Max. Samples', 1, 10000, 100)
      .onChange(controls.maxSamplesChanged)

    playback.open()
    settings.open()
  }

  controls.start = function () {
    window.dispatchEvent(new CustomEvent('toggle-planning'))
  }

  controls.reset = function () {
    window.dispatchEvent(new CustomEvent('reset-planning'))
  }

  controls.deltaChanged = function () {
    var data = {
      detail: {
        delta: controls[ 'Delta' ]
      }
    }
    window.dispatchEvent(new CustomEvent('delta-changed', data))
  }

  controls.volumeSizeChanged = function () {
    var data = {
      detail: {
        volumeSize: controls[ 'Volume Size' ]
      }
    }
    window.dispatchEvent(new CustomEvent('volume-size-changed', data))
  }

  controls.maxSamplesChanged = function () {
    var data = {
      detail: {
        maxSamples: controls[ 'Max. Samples' ]
      }
    }
    window.dispatchEvent(new CustomEvent('max-samples-changed', data))
  }

  init.call(this)
}
