'use strict'

function RRT (dim, nodeCallback, collisionCheckerObj) {
  var addConfigurationCB = nodeCallback
  var collisionChecker = collisionCheckerObj
  var finishedCB = null
  var dimension = dim

  var delta = null
  var maxPlanningSteps = null
  var planningStepsCount = null

  var vertices = []

  function randomNumber () {
    // get random value between -dimension/2 and +dimension/2
    // first stretch value from 0..1 to 0..dimension, then subract dimension/2
    return Math.floor((Math.random() * dimension) - (dimension / 2))
  }

  function randomConfiguration () {
    return new THREE.Vector3(
      randomNumber(),
      randomNumber(),
      randomNumber()
    )
  }

  function nearestConfiguration (node) {
    var nearest = null
    var nearestDistance = dimension * dimension
    vertices.forEach(function (vertex) {
      var dist = node.distanceTo(vertex)
      if (dist < nearestDistance) {
        nearest = vertex
        nearestDistance = dist
      }
    })
    return nearest
  }

  function newConfiguration (near, random) {
    // go one delta from near towards random
    var direction = random.sub(near)
   // don't modify the original near vector
    return near.clone().addScaledVector(direction.normalize(), delta)
  }

  function plan (qInit, maxNodeCount, extensionDelta) {
    init(qInit, maxNodeCount, extensionDelta)
    while (planningStepsCount < maxPlanningSteps) {
      planStep()
    }
  }

  function init (qInit, maxSamples, extensionDelta) {
    vertices = [qInit]
    delta = extensionDelta
    maxPlanningSteps = maxSamples
    planningStepsCount = 0
  }

  function planStep () {
    if (planningStepsCount < maxPlanningSteps) {
      var randomNode = randomConfiguration()
      var nearestNode = nearestConfiguration(randomNode)
      var newNode = newConfiguration(nearestNode, randomNode)
      if (!collisionChecker.hasCollision(newNode)) {
        vertices.push(newNode)
        addConfigurationCB(newNode, nearestNode)
      }
      planningStepsCount++
    } else {
      if (finishedCB) {
        finishedCB()
      }
    }
  }

  function setDelta (extensionDelta) {
    delta = extensionDelta
  }

  function setMaxSamples (maxSamples) {
    maxPlanningSteps = maxSamples
  }

  function setVolumeSize (newSize) {
    dimension = newSize
  }

  return {
    init: init,
    plan: plan,
    planStep: planStep,
    setDelta: setDelta,
    setMaxSamples: setMaxSamples,
    setVolumeSize: setVolumeSize
  }
}
