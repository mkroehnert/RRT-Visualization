'use strict'

// export
//  https://github.com/spite/ccapture.js/
//  http://www.tysoncadenhead.com/blog/exporting-canvas-animation-to-mov

function RrtApp () {
  var data = {
    gui: null,
    planning: false,
    scene: null,
    camera: null,
    renderer: null,
    controls: null,
    rrtGroup: null,
    bbox: null,
    bboxMesh: null,
    obstacles: [],
    rrt: null,
    rrtCollisionChecker: null,
    lastLine: null,
    qInit: new THREE.Vector3(0, 0, 0),
    material: {
      line: new THREE.LineBasicMaterial({
        color: 0x0000ff,
        linewidth: 1.5 }),
      newLine: new THREE.LineBasicMaterial({
        color: 0x00ff00,
        linewidth: 1.5 })
    }
  }

  var init = function () {
    // create Three.js scene
    data.scene = new THREE.Scene()

    // create a camera
    var aspectRation = window.innerWidth / window.innerHeight
    data.camera = new THREE.PerspectiveCamera(75, aspectRation, 0.1, 1000)

    // create an event listener that resizes the renderer with the browser window.
    data.setEventListeners()

    // create WebGL renderer
    data.renderer = new THREE.WebGLRenderer()
    data.renderer.setSize(window.innerWidth, window.innerHeight)
    data.renderer.setClearColor(0x999999)

    document.getElementById('container').appendChild(data.renderer.domElement)

    // create gui for changing parameters
    data.gui = new RrtGui()

    // add controls for rotating and zooming the scene with the mouse
    data.controls = new THREE.OrbitControls(data.camera, data.renderer.domElement)

    // draw coordinate axes at (0, 0, 0)
    var axisHelper = new THREE.AxesHelper(3)
    data.scene.add(axisHelper)

    // draw bounding box
    data.bboxMesh = new THREE.Mesh(new THREE.BoxGeometry(1, 1, 1), new THREE.MeshBasicMaterial(0xffffff))
    data.bbox = new THREE.BoxHelper(data.bboxMesh, 0x000000)
    data.scene.add(data.bbox)

    // add obstacles
    var box1 = new THREE.Box3()
    box1.setFromCenterAndSize(new THREE.Vector3(5, 10, 5), new THREE.Vector3(4, 4, 4))
    data.obstacles[0] = new THREE.Box3Helper(box1, 0xffffff)

    var box2 = new THREE.Box3()
    box2.setFromCenterAndSize(new THREE.Vector3(-5, 0, -5), new THREE.Vector3(5, 40, 5))
    data.obstacles[1] = new THREE.Box3Helper(box2, 0xffffff)

    var box3 = new THREE.Box3()
    box3.setFromCenterAndSize(new THREE.Vector3(-7, 0, 7), new THREE.Vector3(7, 7, 7))
    data.obstacles[2] = new THREE.Box3Helper(box3, 0xffffff)

    var box4 = new THREE.Box3()
    box4.setFromCenterAndSize(new THREE.Vector3(16, -16, 0), new THREE.Vector3(8, 8, 40))
    data.obstacles[3] = new THREE.Box3Helper(box4, 0xffffff)

    data.obstacles.forEach(obstacle => data.scene.add(obstacle))

    // create group for easy deletion of rendered RRT
    data.rrtGroup = new THREE.Group()
    data.scene.add(data.rrtGroup)

      // create collision checker instance
    data.rrtCollisionChecker = RrtCollisionChecker(data.obstacles)

    // create RRT instance
    data.rrt = RRT(data.gui.getVolumeSize(), data.newConfigurationCB, data.rrtCollisionChecker)

    // reset/initialize all elements
    data.resetPlanning()

    // start rendering
    data.render()
  }

  data.setEventListeners = function () {
    window.addEventListener('resize', data.onWindowResize)

    window.addEventListener('toggle-planning', data.togglePlanning)
    window.addEventListener('reset-planning', data.resetPlanning)
    window.addEventListener('delta-changed', data.onDeltaChanged)
    window.addEventListener('volume-size-changed', data.onVolumeSizeChanged)
    window.addEventListener('max-samples-changed', data.onMaxSamplesChanged)
  }

  data.render = function () {
    requestAnimationFrame(data.render)

    if (data.planning) {
      data.rrt.planStep()
    }

    data.renderer.render(data.scene, data.camera)
    data.controls.update()
  }

  data.togglePlanning = function () {
    data.planning = !data.planning
  }

  data.pausePlanning = function () {
    data.planning = false
  }

  data.resetPlanning = function () {
    // pause planning
    data.pausePlanning()
    // reset RRT
    data.rrt.init(data.qInit, data.gui.getMaxSamples(), data.gui.getDelta())
    // reset visualization by iterating over a copy of the elements to remove
    data.rrtGroup.children.slice().forEach(function (element) {
      // element.material.dispose();
      element.geometry.dispose()
      data.rrtGroup.remove(element)
    })
    // reset bounding box
    data.bboxResize()
    // reset camera position and orientation
    data.camera.lookAt(0, 0, 0)
    data.camera.position.set(0, 0, 50)
  }

  data.newConfigurationCB = function (node1, node2) {
    if (data.lastLine) {
      data.lastLine.material = data.material.line
    }
    data.lastLine = createLine(node1, node2)
    data.rrtGroup.add(data.lastLine)
  }

  data.bboxResize = function () {
    var bboxSize = data.gui.getVolumeSize()
    data.bboxMesh.scale.set(bboxSize, bboxSize, bboxSize)
    data.bbox.update()
  }

  data.onWindowResize = function () {
    var WIDTH = window.innerWidth,
      HEIGHT = window.innerHeight
    data.renderer.setSize(WIDTH, HEIGHT)
    data.camera.aspect = WIDTH / HEIGHT
    data.camera.updateProjectionMatrix()
  }

  data.onDeltaChanged = function (event) {
    data.rrt.setDelta(event.detail.delta)
  }

  data.onVolumeSizeChanged = function (event) {
    // change volume size of RRT
    data.rrt.setVolumeSize(event.detail.volumeSize)
    // reset bounding box
    data.bboxResize()
  }

  data.onMaxSamplesChanged = function (event) {
    data.rrt.setMaxSamples(event.detail.maxSamples)
  }

  function createNode (position) {
    var radius = 0.25
    var segmentCount = 32
    var sphereGeom = new THREE.SphereGeometry(radius, segmentCount)
    var sphereMaterial = new THREE.MeshBasicMaterial({color: 0xff0000})
    var sphere = new THREE.Mesh(sphereGeom, sphereMaterial)
    return sphere
  }

  function createLine (pos1, pos2) {
    var geometry = new THREE.Geometry()
    geometry.vertices.push(
      pos1.clone(),
      pos2.clone()
    )
    return new THREE.Line(geometry, data.material.newLine)
  };

  init.call(this)
}
