'use strict'

window.addEventListener('load', function (event) {
  if (!Detector.webgl) {
    var warning = Detector.getWebGLErrorMessage()
    document.getElementById('container').appendChild(warning)
    return
  }

  serviceWorkerInit()
  var app = new RrtApp()
})

// Service Worker

function serviceWorkerInit () {
    // FF debug page:  about:debugging#workers
  if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/RRT-Visualization/serviceworker.js', {scope: '/RRT-Visualization/'})
    .then(function (reg) {
      // registration worked
      console.log('Registration succeeded. Scope is ' + reg.scope)
    }).catch(function (error) {
      // registration failed
      console.log('Registration failed with ' + error)
    })
  }
}

// http://stackoverflow.com/questions/979975/how-to-get-the-value-from-the-get-parameters/979995#979995
function getQuery () {
  // the return value is assigned to queryDict
  var queryDict = {}
  var query = window.location.search.substring(1)
  var vars = query.split('&')
  for (var i = 0; i < vars.length; i++) {
    var pair = vars[i].split('=')
    // If first entry with this name
    if (typeof queryDict[pair[0]] === 'undefined') {
      queryDict[pair[0]] = decodeURIComponent(pair[1])
    // If second entry with this name
    } else if (typeof queryDict[pair[0]] === 'string') {
      var arr = [ queryDict[pair[0]], decodeURIComponent(pair[1]) ]
      queryDict[pair[0]] = arr
    // If third or later entry with this name
    } else {
      queryDict[pair[0]].push(decodeURIComponent(pair[1]))
    }
  }
  return queryDict
};
