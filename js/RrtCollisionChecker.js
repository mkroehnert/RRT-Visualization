'use strict'

function RrtCollisionChecker (obstacles) {
  var obstacleList = obstacles || []

  function hasCollision (node) {
    return obstacleList.some(obstacle => {
      return obstacle.box.containsPoint(node)
    })
  }

  return {
    hasCollision: hasCollision
  }
}
