# RRT Visualization

This project visualizes the exploration of a cubic search space by a single RRT.
You can see how the search tree grows starting in the middle of the cube.
Parameters can be adjusted while the planning algorithm is executing.

Visit https://mkroehnert.gitlab.io/RRT-Visualization/ in order to see the demo.

# Rapidly exploring Random Trees (RRTs)

Rapidly-exploring Random Tree (RRT) algorithms are used for motion planning in robotics.
The original RRT algorithm was developed by Steven M. LaVall and James J. Kuffner Jr[^1][^2].
RRTs efficiently handle high-dimensional search spaces including obstacles.
They explore a given search space by expanding in the direction of randomly chosen samples and are biased to grow into not yet visited areas.

# Used Open Source Libraries #

* [three.js](https://threejs.org)
* [dat.gui](https://github.com/dataarts/dat.gui)

# License

The source code of this RRT demo is available under the MIT license.
See [LICENSE](LICENSE) for details.

 [^1]: LaValle, Steven M. "Rapidly-exploring random trees: A new tool for path planning," (October 1998). Technical Report. Computer Science Department, Iowa State University (TR 98-11). http://msl.cs.uiuc.edu/~lavalle/papers/Lav98c.pdf  http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.35.1853

 [^2]: Steven M. LaValleJames J. Kuffner, Jr. ""Randomized Kinodynamic Planning," The International Journal of Robotics Research, Vol 20, Issue 5, pp. 378 - 400. http://msl.cs.uiuc.edu/~lavalle/papers/LavKuf01b.pdf https://dx.doi.org/10.1177%2F02783640122067453
